/**
 * The MIT License (MIT) 
 *
 * Copyright (c) 2015, Sergi LLamas Xart, Josep Sánchez Ferreres
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
*/

grammar Arl;

// #####################
// #      HEADER       #
// #####################

options {
    output = AST;
    ASTLabelType = ArlTree;
}

tokens {
    ROOT;
    FUNCTION_CALL;
    PARAM_LIST;
    ARRAY_T;
    ARRAY_V;
    MAP_V;
    EMPTY_CONTAINER;
    DIMENSION; 
    DECLARATION;
    PARAM_LIST;
    INSTR_LIST;
    ATN_BODY;
    TRANSITIONS;
    TRANSITION;
    GLOBAL_VAR;
    UNARYPLUS;
}

@header {
package parser;
import interp.ArlTree;
}

@lexer::header {
package parser;
}

// #####################
// #      GRAMMAR      #
// #####################


prog: root -> ^(ROOT root);
root: (atn_declaration | func_declaration| global_var_decl)* EOF!;

// ====== DECLARATIONS =======

// variables
global_var_decl: var_decl SP -> ^(GLOBAL_VAR var_decl);
atn_var_decl: var_decl SP!;
var_decl: type ID (ASSIGN expr)? -> ^(DECLARATION type ID expr?);

// functions
func_declaration: FUNC^ ID params (ARROW! type)? instr_body;
params: '(' (type ID (',' type ID)*)? ')' -> ^(PARAM_LIST (type ID)*);

// ATNs
atn_declaration: ATN^ ID '{'! atn_declbody '}'!;
atn_declbody: states_decl (i+=atn_var_decl | i+=state_def)* 
    -> ^(ATN_BODY states_decl $i*);

// ATN states
states_decl: STATES '{' (i+=ID (',' i+=ID)*)? '}' 
             INI '{' (j+=ID (',' j+=ID)*)? '}' 
             ACCEPT '{' (k+=ID (',' k+=ID)*)? '}'
    ->
    ^(STATES $i*) ^(INI $j*) ^(ACCEPT $k*);

state_def: LOGIC? STATE^ ID '{'! state_body '}'!;
state_body: transition* state_actions? 
    -> ^(TRANSITIONS transition*) ^(ACTIONS state_actions?);
transition: expr ARROW ID instr_body? -> ^(TRANSITION expr ID instr_body?);
state_actions: ACTIONS! instr_body;

// ======= INSTRUCTIONS =======

instr_body: '{' instr* '}' -> ^(INSTR_LIST instr*);
instr: ((var_decl | assign | funcall | special_instr) SP!) | control_instr;

// special instructions like write
special_instr: WRITE^ expr 
             | CALL^ ID (atom)?;

// assignment
assign: variable ASSIGN^ expr;

// flow control instructions
control_instr: if_instr | while_instr | (return_instr SP!);
if_instr: IF^ '('! expr ')'! instr_body else_if_instr* else_instr?;
else_if_instr: ELSE! IF! '('! expr ')'! instr_body;
else_instr: ELSE! instr_body;
while_instr: WHILE^ '('! expr ')'! instr_body;
return_instr: RETURN^ expr;

// action call (void function call)
funcall: ID params_call -> ^(FUNCTION_CALL ID params_call);
params_call: '(' (expr (',' expr)*)? ')' -> ^(PARAM_LIST expr*);

// ======= TYPE SPECIFIERS =======

/*
type @init{boolean isArray = false;}:
    t=noarray_type ({isArray=true;}array_specifier)? 
        -> {isArray}? ^(ARRAY_T $t array_specifier)
        -> $t;

// array types
noarray_type: (TYPE | MAP^ '('! TYPE ','! type ')'!);
array_specifier: array_specifier_nodim;
array_specifier_nodim: OA^ ']'! array_specifier_nodim? | array_specifier_dim;
array_specifier_dim: OA^ numexpr ']'! array_specifier_dim?;
*/
type: noarray_type (brackets^)*;
brackets: '[' ']' -> ARRAY_T;
noarray_type: (TYPE | MAP^ '('! TYPE ','! type ')'!);

// ======= EXPRESSIONS ======== 
expr: boolterm ( OR^ boolterm )* | mult_dim_lit;
boolterm: notfact ( AND^ notfact)*;
notfact: NOT^ notfact | boolfact;
boolfact: numexpr ( CMP^ numexpr)*;
numexpr: term ( options {greedy=true;} : (PLUS^ term) )*;
term: factor ( options {greedy=true;} : (DOT^ factor) )*;
factor: PLUS^ factor | atom;
atom : '('! expr ')'! | funcall | variable | integral_lit | CAPTURE | REGEX | special_keyword | cast;

//type casting is done as prefix, high precedence operator 
//C style, but with the cast keyword being explicit
cast: CAST^ '('! type ')'! atom;

variable: ID^ ('['! expr ']'!)*;

// ======= SPECIAL KEYWORDS =======
special_keyword : SIZE^ '('! (ID | mult_dim_lit) ')'!
                | INPUT^ '('! expr ')'!
                | EXISTS^ '('! (INPUT | ID) ','! expr ')'!
                | CALL^ ID (atom)?;

// ======= LITERALS =======
integral_lit : INT | FLOAT | BOOL | STRING;
mult_dim_lit: '[' (expr 
            ( ARROW expr (',' expr ARROW expr)* ']' 
              -> ^(MAP_V ^(ARROW expr expr)+)
            | (',' expr)* ']'
                -> ^(ARRAY_V expr+)
            )
            | ']' -> ^(EMPTY_CONTAINER)
            );

// -----------------------
// ======== TOKENS =======
// -----------------------

//operadors
OR : 'or';
AND : 'and';
NOT : 'not';

PLUS : '+'|'-';
DOT : '*'|'/'|'%';
CMP : '<'|'<='|'>'|'>='|'=='|'/='|'!=';
ASSIGN: '=';

//tipus
TYPE : 'int'|'float'|'bool'|'string';
MAP : 'map';
INT : ('0'..'9')+;
//FLOAT : '-'?('0'..'9')+('.'('0'..'9')*)?;
FLOAT : (('0'..'9')+ ((('.' ('0'..'9')*)? ('e' ('+'|'-')? ('0'..'9')+ ))
        | ('.' ('0'..'9')*)) | (('.' ('0'..'9')+) ('e' ('+'|'-')? ('0'..'9')+ )?));
BOOL : 'true'|'false';
FUNC : 'func';
CAPTURE : '$'INT;

ARROW : '->';
SP : ';'+;

//keywords
INPUT: 'input';
CALL: 'call';

ATN: 'atn';
ACTIONS : 'actions';
STATES : 'states';
INI : 'initial';
ACCEPT : 'accepting';
LOGIC : 'logic';
STATE : 'state';

//keywords - special actions
WRITE : 'write';
SIZE  : 'size';
EXISTS: 'exists';
CAST: 'as';

//keywords - control flow
IF  	: 'if' ;
ELSE	: 'else' ;
WHILE	: 'while' ;
RETURN	: 'return' ;

//id
ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')* ;

//regex
REGEX : '#/' (~('/'))* '/';    //'#/'( ESC_SEQ | ~('\\'|'/') )* '/';

//other tokens
OA : '[';

// C-style comments
COMMENT	: '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    	| '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    	;

// Strings (in quotes) with escape sequences        
STRING  :  '"' ( ESC_SEQ | ~('\\'|'"') )* '"'
        ;

fragment
ESC_SEQ
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    ;

// White spaces
WS  	: ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) {$channel=HIDDEN;}
    	;
