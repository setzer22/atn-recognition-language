package interp;

import parser.*;

public class Symbol {
    public String id;
    
    public Symbol() {}
    
    public Symbol(String identifier) {
      id = identifier;
    }
}
