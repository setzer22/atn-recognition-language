package interp;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import parser.*;

public class ArlTreeAdaptor extends CommonTreeAdaptor {
    public Object create(Token t) {
        return new ArlTree(t);
    }

    public Object dupNode(Object t) {
        if ( t==null ) return null;
        return create(((ArlTree)t).token);
    }
    
    public Object errorNode(TokenStream input, Token start, Token stop,
                             RecognitionException e) {
        return null;
    }
}

