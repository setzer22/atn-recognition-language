package interp;

import java.util.regex.*;
import parser.*;

public class RegexData extends Data {
    private static final StringData[] NO_DATA = {};

    public RegexData(String value) {
        this.value = value;
        compiled_regex = Pattern.compile(value);
        last_match = null;
    }

    public String value;
    private Pattern compiled_regex;
    private Matcher last_match;

    @Override
    public Data operation(String op, Data d) {
        assert(d instanceof StringData);

        if(op.equals("==")) {
            last_match = compiled_regex.matcher(((StringData)d).value);
            return new BooleanData(last_match.matches());
        }
        else 
            throw new RuntimeException("Unsupported operator "+op+" for regex type");
    }
    
    public boolean matched() {
        if (last_match == null) return false;
        return last_match.matches();
    }
    
    public boolean hadCaptures() {
        if (!matched()) return false;
        return last_match.groupCount() > 0;
    }
    
    public StringData[] captures() {
        if (last_match == null || !last_match.matches()) return NO_DATA;
        StringData[] captures = new StringData[last_match.groupCount()];
        for(int i = 0; i < last_match.groupCount(); ++i) {
            captures[i] = new StringData(last_match.group(i + 1));
        }
        return captures;
    }

    @Override 
    public Data cast(Type t) {
        switch(t.raw_type) {
            case STRING: return new StringData(value);
            default: throw new RuntimeException("Invalid cast, regex to "+t.toString());
        }
    }
    
    public Data clone() {
      RegexData rd = new RegexData(value);
      rd.last_match = last_match;
      return rd;
    }
}
