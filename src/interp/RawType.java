package interp;

import parser.*;

public enum RawType {
	INTEGER,
	FLOAT,
	BOOLEAN,
	STRING,
	ARRAY,
	REGEX,
	MAP,
    EMPTY_CONTAINER
}

