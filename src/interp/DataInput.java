package interp;

import parser.*;
import java.util.*;
import java.io.*;

public class DataInput {

    private int position;
    private List<Map<String,String>> input;

    public DataInput(List<Map<String,String>> data) {
        input = data;
        position = -1;
    }
    
    public DataInput(ArrayData data) {
        List<Data> dataList = data.value;
        input = new ArrayList<>();
        for (Data map : dataList) {
            Map<String, String> newMap = new HashMap<>();
            MapData mmap = (MapData) map;
            for (Data key : mmap.value.keySet()) {
                StringData skey = (StringData) key;
                StringData sval = (StringData) mmap.value.get(skey);
                newMap.put(skey.value, sval.value);
            }    
            input.add(newMap);
        }
        position = -1;
    }

    public String get(String key) {
        assert(0 <= position && position < input.size());
        return input.get(position).get(key);
    }
    
    // returns the value of the field defined by _DEFAULT_INPUT_FIELD_ or 'text' if not defined
    public String getDefault(SymbolTable s) {
        assert(0 <= position && position < input.size());
        assert(!input.get(position).isEmpty());
        String key;
        Symbol defaultField = s.getGlobalSymbol("_DEFAULT_INPUT_FIELD_");
        
        //get default field if exists and defined as StringData
        if (defaultField != null && defaultField instanceof VariableSymbol && 
           ((VariableSymbol) defaultField).data instanceof StringData) {
            key = ((VariableSymbol) defaultField).data.stringv();
        } else { //else use 'text' field as default
            key = "text";
        }
        
        //if key exists, return key->value
        if (input.get(position).containsKey(key)) {
            return input.get(position).get(key);
        } else {
            throw new RuntimeException("Default field '" + key + "' not found");
        }
    }

    public boolean exists(String key) {
        assert(position < input.size());
        if (position < 0) return false;
        return input.get(position).containsKey(key);
    }
    
    public StringData processRegex(RegexData regex) {
        assert(position < input.size());
        if (position < 0) return null;
        
        // check matches regex-key
        for (String key : input.get(position).keySet()) {
            StringData keyData = new StringData(key);
            regex.operation("==", keyData);
            if (regex.matched())
                return new StringData(input.get(position).get(keyData.value));
        }
        return null;
    }
    
    public void reset() {
        position = -1;
    }

    public boolean advance() {
        if(position >= input.size()-1) {
            return false;
        }
        else {
            position = position + 1; 
            return true;
        }
    }
    
    public boolean back() {
        if(position < 0) {
            return false;
        }
        else {
            position = position - 1;
            return true;
        }
    }

    public boolean at_end() {
        return position >= input.size()-1;
    }
    
    public void printDebug() {
        System.out.println("DataInput contents: ");
        for (Map m : input) {
            System.out.println(m);
        }
    }
    
}
