package interp;

import parser.*;
import java.util.*;
import java.io.*;

/** Class that implements the interpreter of the language. */
public class Interp {
    private History hist;
    private SymbolTable s;
    private StringData[] captures;
    private DataInput input;
    private DataInput originalInput;
    private HashMap<String, ATN> ATNlist;

    /**Utility functions*/
    static <T extends Comparable<T>> T max (T a, T b) { return a.compareTo(b) > 0 ? a : b; }
    static <T extends Comparable<T>> T min (T a, T b) { return a.compareTo(b) > 0 ? b : a; }

    
    public Interp(ArlTree T, SymbolTable symTable, HashMap<String, ATN> ATNlist, String path) {
        captures = new StringData[0];
        if (path != null) {
          originalInput = CSVParser.parse(path);
        } else {
          originalInput = null;
        }
        input = originalInput;
        
        s = symTable;
        this.ATNlist = ATNlist;
        hist = new History(input);
        
        initialize_global_variables(T);
        execute(T);
    }

    /**Starts the interpreter with the parsed program.*/
    public void execute(ArlTree T) {
        ArlTree main = find_execution_handle(T);
        
        if (main == null) { // Execution starting from ATN.
            String st = startingATN(T);
            if (input == null) {
                throw new RuntimeException("No input file specified.");
            }
            if (st.equals("")) {
                throw new RuntimeException("Starting ATN not found.");
            }
            boolean result = atn_start(ATNlist.get(st));
        } else {
            ArlTree instruction_list = main.getChild(main.getChildCount() - 1);
            execute_instruction_list(instruction_list);
        }
    }
    
    private String startingATN (ArlTree T) {
        for (ArlTree t : T.getChildren())
        if (t.getType() == ArlLexer.ATN)
            return t.getChild(0).getText();
        return "";
    }
    
    private boolean atn_start(ATN atn) {
        //execute atn declarations
        for (ArlTree declaration : atn.declarations) {
            VariableSymbol sym = instr_declaration(declaration);
            sym.is_atn_var = true;   
        }
        
        //execute atn starting from every possible init state
        for (String ini : atn.initStates) {
            if (atn_execute(ini, atn, null))
                return true;
        }
        return false;
    }

    private boolean subatn_start(ATN atn) {
        //execute sub-atn declarations
        for (ArlTree declaration : atn.declarations) {
            VariableSymbol sym = instr_declaration(declaration);
            sym.is_atn_var = true;
        }
    
        for (String ini : atn.initStates) {
            if (subatn_execute(ini, atn, null))
                return true;
        }
        return false;
    }

    private boolean atn_execute(String state, ATN atn, ArlTree transitionActions) {
        boolean is_logic = atn.logicStates.contains(state);

        ArlTree stateActions = atn.statesActions.get(state);

        // States at the end of the input or logic ones don't
        // make the input advance
        hist.push((input.at_end() || is_logic) ? 0 : 1);

        if (transitionActions != null) 
            execute_instruction_list(transitionActions);
        if (stateActions != null) 
            execute_instruction_list(stateActions);

        hist.merge_if_requested();

        //handle logic states
        boolean input_left = true;
        if(!is_logic) 
            input_left = input.advance();

        boolean success = false;
        if (atn.acceptingStates.contains(state)) {
            success = true;
        }
        else if(!input_left) {}
        else if(atn.transitionsTable.get(state) == null) {}
        else {
            for (Transition t : atn.transitionsTable.get(state)) {
                if (success) break;
                if (evaluate_expression(t.condition).boolv()) 
                    success = atn_execute(t.nextState, atn, t.actions);
            }
        }

        if(success) { //Execution was fine
        }
        else { //No transition lead to success, backtrack to the upper state
            hist.pop_undo();
        }
        return success;
    }
    
    private boolean subatn_execute(String a, ATN b, ArlTree c) {return subatn_execute(a,b,c,0);}
    private boolean subatn_execute(String state, ATN atn, ArlTree transitionActions, int depth) {
        boolean is_logic = atn.logicStates.contains(state);

        ArlTree stateActions = atn.statesActions.get(state);

        // States at the end of the input or logic ones don't
        // make the input advance
        hist.push((input.at_end() || is_logic) ? 0 : 1);

        if (transitionActions != null) 
            execute_instruction_list(transitionActions);
        if (stateActions != null) 
            execute_instruction_list(stateActions);

        hist.merge_if_requested();

        //handle logic states
        boolean input_left = true;
        if(!is_logic) 
            input_left = input.advance();

        boolean success = false;
        if (atn.acceptingStates.contains(state)) {
            success = true;
        }
        else if(!input_left) {}
        else if(atn.transitionsTable.get(state) == null) {}
        else {
            for (Transition t : atn.transitionsTable.get(state)) {
                if (success) break;
                if (evaluate_expression(t.condition).boolv()) 
                    success = subatn_execute(t.nextState, atn, t.actions, depth+1);
            }
        }

        if(success) { //Execution was fine, pop_merge upward 
            if(depth != 0) hist.pop_merge(); 
            else {
                // The last state is not merged with the last push, 
                // instead, it's merged with the next push that's 
                // going to be made, right after its actions. We
                // achieve this by requesting a merge
                //
                // This ensures that undoing the next state's actions is 
                // also going to undo this subatn's actions altogether
                hist.request_merge();
            }
        }
        else { //No transition lead to success, backtrack to the upper state
            hist.pop_undo();
        }
        return success;
    }
    
    /**Initializes global variables values (they are already in the SymbolTable). */
    private void initialize_global_variables(ArlTree T) {
      for(ArlTree node : T.getChildren()) {
        if (node.getType() == ArlLexer.GLOBAL_VAR) {
          ArlTree decl = node.getChild(0);
          VariableSymbol sym = (VariableSymbol) s.getSymbol(decl.getChild(1).getText());
          if(decl.getChildCount() > 2) {
            sym.data = evaluate_expression(decl.getChild(2));
          }
          else {
            sym.data = Data.get_default_value(sym.type);
          }
        }
      }
    }

    /**Returns the returned data resulting of the execution of function func
       If func is a void function or an instruction list such as an action, this
       function returns null.*/
    private Data execute_instruction_list(ArlTree list) {
        if(list.getChildren() == null) return null;

        for(ArlTree instr : list.getChildren()) { 
            switch(instr.getType()) {
                case ArlLexer.RETURN: 
                    Data return_value = evaluate_expression(instr.getChild(0));
                    return return_value;
                case ArlLexer.DECLARATION:
                    instr_declaration(instr);
                    break;
                case ArlLexer.ASSIGN: 
                    instr_assign(instr);
                    break;
                case ArlLexer.CALL:
                    instr_call(instr);
                    break;    
                case ArlLexer.FUNCTION_CALL:
                    instr_function_call(instr);
                    break;
                case ArlLexer.IF:
                    //This handles a chain of if-elseif*-else
                    int if_count = 0;
                    boolean did_if = false;
                    int max_it = max(2, instr.getChildCount() - 1);
                    //inv: if_count-th child is a condition
                    //     if_count+1-th child is an action list
                    for(; !did_if && if_count < max_it; if_count += 2) {
                        Data condition = evaluate_expression(instr.getChild(if_count));
                        if(condition.boolv()) {
                            s.pushCodeBlock(); 
                            Data nested_return_value = execute_instruction_list(instr.getChild(if_count+1));
                            s.popCodeBlock(); 
                            if (nested_return_value != null) return nested_return_value;
                            did_if = true;
                        }
                    }
                    //If we had an even number of children, the remaining one is the else 
                    if(!did_if && instr.getChildCount() % 2 != 0) {
                        s.pushCodeBlock(); 
                        Data nested_return_value = execute_instruction_list(instr.getChild(instr.getChildCount() - 1));
                        s.popCodeBlock(); 
                        if (nested_return_value != null) return nested_return_value;
                    }
                    break;
                case ArlLexer.WHILE:
                    Data while_condition = evaluate_expression(instr.getChild(0));
                    while (((BooleanData)while_condition).value) {
                        //@Copy-paste: This is copy pasted from IF
                        s.pushCodeBlock(); 
                        Data nested_return_value = execute_instruction_list(instr.getChild(1));
                        s.popCodeBlock(); 
                        if (nested_return_value != null) return nested_return_value;
                        while_condition = evaluate_expression(instr.getChild(0));
                    }
                    break;
                case ArlLexer.WRITE:
                    String out = evaluate_expression(instr.getChild(0)).stringv();
                    System.out.println(out);
                    if(out.equals("@stackPrint"))
                        hist.stackPrint(); 
                    break;
            }
        }
        return null;
    }

    private Data instr_function_call(ArlTree instr) {
        String id = instr.getChild(0).getText();
        FunctionSymbol func = (FunctionSymbol) s.getSymbol(id);
        List<ArlTree> param_list = instr.getChild(1).getChildren();
        s.pushCodeBlock();
            if(param_list != null) {
                int i = 0;
                for(ArlTree param : param_list) {
                    String name = func.param_names.get(i);
                    VariableSymbol param_symbol = new VariableSymbol();
                    param_symbol.id = name;
                    param_symbol.data = evaluate_expression(param);
                    param_symbol.type = func.param_types.get(i);

                    s.defineSymbol(name, param_symbol);
                    ++i; 
                }
            }
            Data d = execute_instruction_list(func.code);
            
        s.popCodeBlock();
        return d;
    }

    /**Auxiliar function - Executes a declaration instruction*/
    private VariableSymbol instr_declaration(ArlTree instr) {
        VariableSymbol symbol = new VariableSymbol();
        symbol.id = instr.getChild(1).getText();
        symbol.type = instr.getChild(0).getTypeValue();
        if(instr.getChildCount() > 2) {
            symbol.data = evaluate_expression(instr.getChild(2));
        }
        else {
            symbol.data = Data.get_default_value(symbol.type);
        }
        s.defineSymbol(symbol.id, symbol);
        hist.saveIfNeeded(symbol);
        return symbol;
    }

    /**Auxiliar function - Executes an assign instruction*/
    private void instr_assign(ArlTree instr) {
        ArlTree variable = instr.getChild(0);
        VariableSymbol symbol = (VariableSymbol) s.getSymbol(variable.getText());
        hist.saveIfNeeded(symbol);

        Data val = evaluate_expression(instr.getChild(1));
        if(variable.getChildCount() == 0) {
            symbol.data = val;
        }
        else {
            //This handles both arrays and maps, which share a common 
            //operation interface.
            Data container = symbol.data;
            Type container_t = symbol.type;
            if(container == null) {
                symbol.data = Data.get_default_value(container_t);
                container = symbol.data;
            }
            int n_dim = variable.getChildCount();
            for(int i = 0; i < n_dim - 1; ++i) {
                Data dim = evaluate_expression(variable.getChild(i));
                container_t = container_t.contained_type;
                container.ternary_operation("ensure", dim, container_t);
                container = container.operation("get", dim);
            }
            Data last_dimension = evaluate_expression(variable.getChild(n_dim - 1));
            container.ternary_operation("ensure", last_dimension, container_t);
            container.ternary_operation("set", last_dimension, val);
        }
    }

    /** Returns the data resulting from evaluating the expression represented by expr*/
    private Data evaluate_expression(ArlTree expr) {
        Data d = null;
        switch(expr.getType()) {
            case ArlLexer.PLUS:
            case ArlLexer.NOT:
            case ArlLexer.DOT:
            case ArlLexer.CMP:
                Data lhs = evaluate_expression(expr.getChild(0));
                Data rhs = null;
                if(expr.getChildCount() >= 2) {
                    rhs = evaluate_expression(expr.getChild(1));
                }
                d = lhs.operation(expr.getText(), rhs);
                if (lhs instanceof RegexData) updateCaptures(lhs);
                if (rhs instanceof RegexData) updateCaptures(rhs);
                break;    
            case ArlLexer.AND:
                BooleanData lhsAND = (BooleanData) evaluate_expression(expr.getChild(0));
                if (!lhsAND.value) d = new BooleanData(false);
                else d = evaluate_expression(expr.getChild(1));
                break;
            case ArlLexer.OR:
                BooleanData lhsOR = (BooleanData) evaluate_expression(expr.getChild(0));
                if (lhsOR.value) d = new BooleanData(true);
                else d = evaluate_expression(expr.getChild(1));
                break;
            case ArlLexer.INT: 
                d = new IntegerData(expr.getIntValue());
                break;
            case ArlLexer.BOOL: 
                d = new BooleanData(expr.getBooleanValue());
                break;
            case ArlLexer.FLOAT: 
                d = new FloatData(expr.getFloatValue());
                break;
            case ArlLexer.EXISTS:
                d = evaluate_exists(expr);
                break;
            case ArlLexer.INPUT:
                Data child = evaluate_expression(expr.getChild(0));
                if (child instanceof StringData) {
                    String key = child.stringv();
                    if(!input.exists(key)) {
                        throw new RuntimeException("Key value '"+ key +"' not defined.");
                    } else {
                        d = new StringData(input.get(key));
                    }                
                } else { // regex case
                    RegexData rd = (RegexData) child;
                    d = input.processRegex(rd);
                    updateCaptures(rd);
                }
                break;
            case ArlLexer.CALL:
                d = instr_call(expr);
                break;    
            case ArlLexer.SIZE:
                Data container = evaluate_expression(expr.getChild(0));
                return container.operation("size", null);
            case ArlLexer.STRING:
                d = new StringData(expr.getStringValue());
                break;
            case ArlLexer.REGEX:
                RegexData rd = new RegexData(expr.getRegexValue());
                if (expr.getImplicitInput()) {
                    d = rd.operation("==", new StringData(input.getDefault(s)));
                    updateCaptures(rd);
                } else d = rd;
                break;
            case ArlLexer.CAPTURE:
                int groupNumber = Integer.parseInt(expr.getText().substring(1)) - 1;
                if (groupNumber >= captures.length) {
                    throw new RuntimeException("Capture group '$"+ (groupNumber + 1) +"' not found. " +
                                               "Number of currently accessible capture groups: " + captures.length);
                }
                d = captures[groupNumber];
                break;
            case ArlLexer.FUNCTION_CALL:
                d = instr_function_call(expr);
                break;
            case ArlLexer.ID:
                Symbol sym = s.getSymbol(expr.getText());
                if (sym instanceof ATNSymbol)
                    d = new BooleanData(subatn_start(ATNlist.get(sym.id)));
                else {
                    VariableSymbol symbol = (VariableSymbol) sym;
                    if(expr.getChildCount() == 0) {
                        d = symbol.data.clone();
                    } 
                    else {
                        d = symbol.data;
                        for(ArlTree dimension : expr.getChildren()) {
                            if(d == null) throw new RuntimeException("Trying to access uninitialized array");
                            d = d.operation("get", evaluate_expression(dimension));
                        }
                        if(d == null) throw new RuntimeException("Trying to access uninitialized array");
                    }
                }
                break;
            case ArlLexer.CAST:
                Type to = expr.getChild(0).getTypeValue();
                Data from = evaluate_expression(expr.getChild(1));
                d = from.cast(to);
                break;
            case ArlLexer.ARRAY_V:
                d = array_literal_to_data(expr);
                break;
            case ArlLexer.MAP_V:
                Map<Data, Data> map = new HashMap<Data, Data>();
                for(ArlTree pair : expr.getChildren()) {
                    Data map_key = evaluate_expression(pair.getChild(0));
                    Data value = evaluate_expression(pair.getChild(1));
                    map.put(map_key, value);
                }
                d = new MapData(map);
                break;
            case ArlLexer.EMPTY_CONTAINER:
                //Let it return null
                break;
        }
        return d;
    }
    
    private Data instr_call(ArlTree expr) {
        String id = expr.getChild(0).getText();
        if (expr.getChildCount() > 1) {
            input = new DataInput((ArrayData) evaluate_expression(expr.getChild(1)));
        } else {
            input = originalInput;
            if (input == null) {
                throw new RuntimeException("In CALL instruction, found input == null.");
            }
            input.reset();
        }
        hist.reset(input);
        return new BooleanData(atn_start(ATNlist.get(id)));
    }
    
    private Data evaluate_exists(ArlTree t) {
        ArlTree child = t.getChild(0);
        if (child.getType() == ArlLexer.INPUT) {
            Data valueToCheck = evaluate_expression(t.getChild(1));
            if (valueToCheck instanceof StringData) {
                return new BooleanData(input.exists(valueToCheck.stringv()));
            } else { // regex case
                RegexData regex = (RegexData) valueToCheck;
                boolean result = input.processRegex(regex) != null;
                updateCaptures(regex);
                return new BooleanData(result);
            }
        } else {
            VariableSymbol sym = (VariableSymbol) s.getSymbol(child.getText());
            MapData map = (MapData) sym.data;
            Boolean bl = map.exists(evaluate_expression(t.getChild(1)));
            return new BooleanData(bl);
        }
    }

    private Data array_literal_to_data(ArlTree expr) {
        List<Data> array = new ArrayList<Data>(expr.getChildCount()); 
        for(ArlTree  child : expr.getChildren()) {
            Data val = evaluate_expression(child);
            array.add(val);
        }
        return new ArrayData(array);
    }
    
    private void updateCaptures(Data regex) {
        if (regex instanceof RegexData && ((RegexData) regex).hadCaptures()) {
            captures = ((RegexData) regex).captures();
        }
    }   

    /**Returns the main function if defined */
    private ArlTree find_execution_handle(ArlTree T) {
        List<ArlTree> children = T.getChildren();
        if(children == null) return null;

        for(ArlTree child : children) {
            switch(child.getType()) {
                case ArlLexer.FUNC: 
                    if(child.getChild(0).getText().equals("main")) return child;
            }
        }
        return null;
    }
}

