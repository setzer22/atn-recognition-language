package interp;

import parser.*;

public class StringData extends Data {

    public StringData(String value) {this.value = value;}
    public String value;

    @Override
    public Data operation(String op, Data d) {
        assert(d instanceof StringData || d instanceof RegexData);

        if(op.equals("+"))
            return new StringData(value + ((StringData)d).value);
        else if(op.equals("==")) {
            if(d instanceof StringData) return new BooleanData(value.equals(((StringData)d).value));
            else { //RegexData
                return ((RegexData)d).operation(op, this);
            }
        }
        else 
            throw new RuntimeException("Unsupported operator "+op+" for string type");
    }
    
    @Override 
    public Data cast(Type t) {
        switch(t.raw_type) {
            case INTEGER: return new IntegerData(Integer.parseInt(value));
            case FLOAT: return new FloatData(Double.parseDouble(value));
            case BOOLEAN: return new BooleanData(Boolean.parseBoolean(value));
            case STRING: return new StringData(value);
            default: throw new RuntimeException("Invalid cast, string to "+t.toString());
        }
    }
    
    public Data clone() {
      return new StringData(value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof StringData)) return false;
        return value.equals(((StringData)o).value);
    }
}
