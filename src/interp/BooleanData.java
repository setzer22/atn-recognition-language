
package interp;

import parser.*;

public class BooleanData extends Data {

    public BooleanData(boolean value) {this.value = value;}

    public boolean value;

    @Override
    public Data operation(String op, Data d) {
        assert(d == null || d instanceof BooleanData);

        if(op.equals("not")) 
            return new BooleanData(!value);
        else 
            throw new RuntimeException("Unsupported operator "+op+" for bool type");
    }

    @Override 
    public Data cast(Type t) {
        switch(t.raw_type) {
            case INTEGER: return new IntegerData(value ? 1 : 0);
            case FLOAT: return new FloatData(value ? 1.0 : 0.0);
            case BOOLEAN: return new BooleanData(value);
            case STRING: return new StringData(Boolean.toString(value));
            default: throw new RuntimeException("Invalid cast, bool to "+t.toString());
        }
    }
    
    public Data clone() {
      return new BooleanData(value);
    }

    @Override
    public int hashCode() {
        return value ? 1 : 0;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof BooleanData)) return false;
        return (new Boolean(value)).equals(((BooleanData)o).value);
    }
}
