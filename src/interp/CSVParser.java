package interp;

import parser.*;
import java.util.*;
import java.io.*;

public class CSVParser {

    private static List<String> splitBackslash(char separator, String text) {
        ArrayList<String> buffer = new ArrayList<>();
        int last_match = 0;
        for (int i = 0; i < text.length(); ++i) {
            if(text.charAt(i) == '\\') 
                i = i + 1;
            else if (text.charAt(i) == separator) {
                buffer.add(text.substring(last_match, i));
                last_match = i + 1;
            }
        }
        
        if (last_match < text.length()) 
            buffer.add(text.substring(last_match, text.length()));
        
        return buffer;
    }

    public static DataInput parse (String path) {
        List<Map<String, String>> lines = new ArrayList<Map<String,String>>();  
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(path));
            String line = reader.readLine();
            while(line != null) {
                if(line.contains("=")) lines.add(process_line(line));
                line = reader.readLine();
            }
            //reader.close() may throw, so we cannot add it in a finally block
            //which should be the proper way to go
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new DataInput(lines);
    }

    private static Map<String,String> process_line(String line) {
        Map<String,String> ret = new HashMap<>();
        List<String> pairs = splitBackslash(',', line);
        for(String pair : pairs) {
            pair = pair.replaceAll("\\\\,", ",");
            List<String> key_val = splitBackslash('=', pair);
            if (key_val.size() == 1) {
                key_val.add("");
            } else if (key_val.size() != 2) {
                throw new RuntimeException("Can't read key-value for pair '"+pair+"' in CSV processing");
            }
            key_val.set(0, key_val.get(0).replaceAll("\\\\=", "="));
            key_val.set(1, key_val.get(1).replaceAll("\\\\=", "="));
            ret.put(key_val.get(0), key_val.get(1));
        }
        return ret;
    }

}


