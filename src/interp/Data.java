package interp;

import parser.*;
import java.util.*;

public abstract class Data {
    public abstract Data operation(String op, Data d); 
    public abstract Data cast(Type t);
    public abstract Data clone();

    //Ternary operators can be overloaded but are not required, also
    //the third operator doesn't have to necessarily be a data, it can
    //also be a type, for example
    public Data ternary_operation(String op, Data key, Object val) {
        return null;    
    }; 
    
    String stringv() {return ((StringData)this).value;}
    boolean boolv() {return ((BooleanData)this).value;}
    int intv() {return ((IntegerData)this).value;}

    public String toString() {
        Type t = new Type(); t.raw_type = RawType.STRING;
        return this.cast(t).stringv();
    }

    /**Returns the default value for a given type t*/
    public static Data get_default_value (Type t) {return get_default_value(t.raw_type);}
    public static Data get_default_value(RawType raw) {
        switch (raw) {
            case INTEGER: return new IntegerData(0);
            case FLOAT: return new FloatData(0);
            case BOOLEAN: return new BooleanData(false);
            case STRING: return new StringData("");
            case MAP: return new MapData(new HashMap<Data, Data>());
            case ARRAY: return new ArrayData(new ArrayList<Data>()); 
            default: throw new RuntimeException("Type not implemented"); 
        }

    }
}
