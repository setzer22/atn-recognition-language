package interp;

import interp.Symbol;
import parser.*;
import java.lang.reflect.*;
import org.antlr.runtime.tree.*;
import org.antlr.runtime.Token;

import java.util.*;
import java.io.*;

/** Class that implements the semantic analysis of the language. */
public class Semantic {

    private HashMap<String, Symbol> symbolTable;
    
    private HashMap<String, ATN> ATNlist;
    
    private boolean atnScope;
    private boolean mainScope;
    private boolean globError;

    private SymbolTable s;
    
    public Semantic(ArlTree psr, SymbolTable st, HashMap<String, ATN> ATNlist) {
        ArlTree T = getRoot(psr);
        s = st;
        mainScope = false;
        atnScope = false;
        globError = false;
        
        precalculate_type_specifiers(T);
        define_global_names(T);
        check_types_and_symbols(T);
        
        this.ATNlist = ATNlist;
        preprocess_ATN(T);
        
        precalculate_constants(T);
        
        if (ATNlist.size() == 0 && s.getSymbol("main") == null)
          new SemanticError("Program should have at least one ATN or main function.", T);

        if (globError) System.exit(0);
    }

    /**Searches for all literals and / or constants and precalculates
       their value so it doesn't happen at runtime.*/
    private void precalculate_constants(ArlTree T) {
        switch (T.getType()) {
        case ArlLexer.INT:
            T.setIntValue();
            break;

        case ArlLexer.FLOAT:
            T.setFloatValue();
            break;

        case ArlLexer.BOOL:
            T.setBooleanValue();
            break;

        case ArlLexer.STRING:
            T.setStringValue();
            break;

        case ArlLexer.REGEX:
            T.setRegexValue();
            break;

        default:
            List<ArlTree> children = T.getChildren();
            if(children != null) {
            for(ArlTree t : T.getChildren()) precalculate_constants(t);
            }
        }
    }

    /** Traverses the AST looking for nodes that represent a type, then cuts that subtree
     *  and adds a leaf holding a Type instance 
     *
     *  @see build_type*/
    private void precalculate_type_specifiers(ArlTree T) {
        switch (T.getType()) {
        case ArlLexer.TYPE:
        case ArlLexer.ARRAY_T:
        case ArlLexer.MAP:
            Type type = build_type(T);
            T.setTypeValue(type); //This prunes the tree and stores the type to the node 
            break;

        default:
            List<ArlTree> children = T.getChildren();
            if(children != null) {
            for(ArlTree t : T.getChildren()) precalculate_type_specifiers(t);
            }
        }
    }
    
    /** Traverses the AST searching for global names, i.e. functions and ATNs and adds them to 
     *  the symbol table. This ensures that you can call a function, or ATN before it's declared*/
    private void define_global_names(ArlTree T) {
      List<ArlTree> children = T.getChildren();
      if (children != null) {
        for(ArlTree t : children) {
          switch (t.getType()) {
          case ArlLexer.ATN:
            String id = t.getChild(0).getText();
            if(s.getSymbol(id) != null) {
              //Symbol already defined
              new SemanticError("Can't define ATN '"+id+"' because it's already defined.", t, true);
              globError = true;
            } else {
              ATNSymbol atnSym = new ATNSymbol(id);
              s.defineGlobalSymbol(atnSym.id, atnSym);
            }
            break;
          case ArlLexer.FUNC:
            define_function(t);
            break;
          case ArlLexer.GLOBAL_VAR:
            ArlTree decl = t.getChild(0);
            VariableSymbol sym = new VariableSymbol();
            sym.id = decl.getChild(1).getText();
            sym.type = decl.getChild(0).getTypeValue(); 
            sym.is_global = true;
            if(decl.getChildCount() == 3) { //We have an initial value
              Type ini_type = check_expression(decl.getChild(2));
              if(!sym.type.equals(ini_type)) {
                new SemanticError("Trying to initialize a variable of type " + sym.type.toString() + 
                                  " with type "+ini_type.toString(), decl, true);
                globError = true;
              }
            } else {
              // definition of default values for every type is done in interpreter
            }
            s.defineGlobalSymbol(sym.id, sym);
            break;
          default:
            // nothing else can be global
            break;
          }
        }
      }
    }

    /**For every ATN in the AST, preprocess and store the data of its states. */
    private void preprocess_ATN(ArlTree T) {
      List<ArlTree> children = T.getChildren();
      if(children != null) {
        for(ArlTree t : children)
          if(t.getType() == ArlLexer.ATN) {
            ATN ATNData = getATN(t.getChild(1));
            ATNlist.put(t.getChild(0).getText(), ATNData);
          }
      }
    }
    
    /**Given an ATN_body node returns a new ATN data structure initialized with
       its corresponding values. */
    /**Pre: T is an ATN_body node */
    private ATN getATN(ArlTree T) {
      ATN atnData = new ATN();
      atnData.transitionsTable = new HashMap<String, ArrayList<Transition>>();
      atnData.declarations = new ArrayList<ArlTree>();
      atnData.statesActions = new HashMap<String, ArlTree>();
      atnData.logicStates = new ArrayList<String>();
      List<ArlTree> children = T.getChildren();
      if (children != null) {
        for(ArlTree t: children) {
            switch (t.getType()) {
            case ArlLexer.STATES:
                atnData.states = new ArrayList<String>();
                List<ArlTree> childrenStates = t.getChildren();
                if (childrenStates != null) 
                  for(ArlTree ts: childrenStates)
                    atnData.states.add(ts.getText());
                break;
            case ArlLexer.INI:
                atnData.initStates = new ArrayList<String>();
                List<ArlTree> childrenIniStates = t.getChildren();
                if (childrenIniStates != null)
                    for(ArlTree ts: childrenIniStates)
                        atnData.initStates.add(ts.getText());
                break;
            case ArlLexer.ACCEPT:
                atnData.acceptingStates = new ArrayList<String>();
                List<ArlTree> childrenAccStates = t.getChildren();
                if (childrenAccStates != null)
                    for(ArlTree ts: childrenAccStates)
                        atnData.acceptingStates.add(ts.getText());
                break;
            case ArlLexer.STATE:
                ArrayList<Transition> transitions = getStateTransitions(t);
                if(t.getLogicState()) {
                    atnData.logicStates.add(t.getChild(0).getText());
                }
                atnData.transitionsTable.put(t.getChild(0).getText(), transitions);
                //add actions for that state
                List<ArlTree> childrenActions = t.getChildren();
                if (childrenActions != null)
                    for(ArlTree k: childrenActions)
                        if (k.getType() == ArlLexer.ACTIONS)
                            atnData.statesActions.put(t.getChild(0).getText(), k.getChild(0));
                break;
            case ArlLexer.DECLARATION:
                atnData.declarations.add(t);
                break;
            default:
                //default
                break;
            }
        }
      }
      return atnData;
    }
    
    /**Returns the list of transitions for the state in the node T. */
    private ArrayList<Transition> getStateTransitions(ArlTree T) {
        assert(T.getType() == ArlLexer.STATE);

        ArrayList<Transition> transitionsList = new ArrayList<Transition>();
        List<ArlTree> children = T.getChildren();

        if (children != null) {
            for(ArlTree transitions_node : children) {
                if (transitions_node.getType() == ArlLexer.TRANSITIONS) {
                    List<ArlTree> transitions = transitions_node.getChildren();
                    if (transitions != null && transitions.size() > 0) {
                        for (ArlTree transition : transitions) {
                            Transition tr = new Transition();
                            tr.nextState = transition.getChild(1).getText();
                            tr.condition = transition.getChild(0);
                            if (transition.getChildCount() == 3) {
                                tr.actions = transition.getChild(2);
                            }
                            transitionsList.add(tr);
                        }
                    }
              }
            }
        }
        return transitionsList;
    }
    
    /**Checks all expressions in the AST are assigned to its correct 
       type, also checks for undeclared symbols; throws errors if found.*/
    private void check_types_and_symbols(ArlTree T) {
        //The root node doesn't need to be checked
        List<ArlTree> children = T.getChildren();
        if(children != null) {
        for(ArlTree t : T.getChildren()) check_types_recursive(t);
        }
    }

    /**Helper function for check_types_and_symbols
     *
     * @see check_types_and_symbols*/
    private void check_types_recursive(ArlTree T) {
        switch(T.getType()) {
        case ArlLexer.FUNC:
            check_function(T);
            break;
        case ArlLexer.ATN:
            check_atn(T);
            break;
        default:
          //GLOBAL_VARS already defined in define_global_names
          break;
        }
    }

    /**Performs semantic checking on an instruction block*/
    private void check_instruction_block(ArlTree block, Type expected_return_type) {
        assert(block.getType() == ArlLexer.INSTR_LIST);
        if(block.getChildren() != null) {
            for(ArlTree instr : block.getChildren()) {
                if(instr.getType() == ArlLexer.RETURN) {
                    if(expected_return_type == null) {
                        new SemanticError("This action block shouldn't be returning anything.", instr);
                    }   
                    check_return_instruction(instr, expected_return_type);
                }
                else 
                    check_instruction(instr, expected_return_type);
            }
        }
    }

    /**Checks the return instruction of a function block.*/
    private void check_return_instruction(ArlTree statement, Type expected_return_type) {
        assert(statement.getType() == ArlLexer.RETURN);
        Type ret_t = check_expression(statement.getChild(0));
        if(!ret_t.equals(expected_return_type)) {
            new SemanticError("This function's return type is "+expected_return_type.toString()+
                                    " but this return statement is returning "+ret_t.toString(), statement);
        }
    }

    /**Adds a function symbol and signature to the symbol table*/
    private void define_function(ArlTree T) {
        String id = T.getChild(0).getText();
        if(s.getSymbol(id) != null) {
            //Symbol already defined
            new SemanticError("Can't define function "+id+" because it's already defined.", T);
        }
        ArlTree params = T.getChild(1);
        if(id.equals("main")) {
            if (params.getChildCount() > 0) {
                new SemanticError("Main can't have arguments.", T, true);
                globError = true;
            }
        }

        List<Type> param_types = new ArrayList<Type>();
        List<String> param_names = new ArrayList<String>();
        for(int i = 0; i < params.getChildCount(); i += 2) {

            ArlTree param_id = params.getChild(i+1);
            assert(param_id != null);

            if(param_names.contains(param_id.getText())) {
                new SemanticError("Redefinition of parameter "+param_id.getText()+" in parameter list.", param_id);
            }

            Symbol param_s = s.getSymbol(param_id.getText());
            if(param_s != null && param_s instanceof FunctionSymbol || param_id.getText().equals(id)) {
              new SemanticError("Argument "+param_id+ " has the same name as an already defined function.", T);
            }
            
            if(param_s != null && param_s instanceof ATNSymbol) {
              new SemanticError("Argument "+param_id+ " has the same name as an already defined ATN.", T);
            }

            param_names.add(param_id.getText());
            Type param_type = params.getChild(i).getTypeValue();
            param_types.add(param_type);
        }
        Type return_type = null;
        if(T.getChildCount() == 4) return_type = T.getChild(2).getTypeValue();

        FunctionSymbol function_sym = new FunctionSymbol();
        function_sym.id = id;
        function_sym.param_names = param_names;
        function_sym.param_types = param_types;
        function_sym.return_type = return_type;
        function_sym.code = T.getChild(T.getChildCount() - 1);
        s.defineGlobalSymbol(id, function_sym);
    }

    /**Finds the return statement in an instruction, and any block it creates*/
    private ArlTree find_return (ArlTree instr) {
       switch(instr.getType()) {
            case ArlLexer.RETURN:
                return instr;
            case ArlLexer.IF:
            case ArlLexer.WHILE:
                for(ArlTree t : instr.getChildren()) {
                    if(t.getType() == ArlLexer.INSTR_LIST &&
                       t.getChildCount() > 0) {
                        for(ArlTree t2 : t.getChildren())
                            return find_return(t2);
                    }
                }
                return null;
            default: return null;
       }
    }

    /**Checks a function declaration
        @pre: This function's name and signature are already defined 
              by define_global_names
    */
    private void check_function(ArlTree T) {
        ArlTree params = T.getChild(1);
        Type expected_return_type = null;

        if (T.getChild(0).getText().equals("main")) {
            mainScope = true;
        }
        
        //If we have a return type
        if(T.getChildCount() == 4) { 
            assert(T.getChild(2) != null && T.getChild(2).getTypeValue() != null);
            Type t = T.getChild(2).getTypeValue(); //Declared return type
            ArlTree ret_node = null;
            if(T.getChild(3).getChildren() != null) {
                for(ArlTree instr : T.getChild(3).getChildren()) {  
                    ret_node = find_return(instr);
                    if(ret_node != null) break;
                }
            }
            if(ret_node == null) {
                new SemanticError("Function's return type is declared as "+t.toString()+
                                        " but the function body doesn't have any return statement", T);
            }
            expected_return_type = t;
        }
        //The return type is not the last node in the ast so we have to do this
        int instr_child_num = T.getChildCount() == 4 ? 3 : 2;

        //Push and define the parameter types
        s.pushCodeBlock();
            //Define params as symbols in current context
            for(int i = 0; i < params.getChildCount(); i += 2) {
                //params.getChild(i) is a type node so we can safely retrieve its type
                VariableSymbol sym = new VariableSymbol();
                sym.is_parameter = true;
                Type param_type = params.getChild(i).getTypeValue();
                sym.type = param_type;
                sym.id = params.getChild(i+1).getText();
                s.defineSymbol(sym.id, sym);
            }

            //Check instruction list
            check_instruction_block(T.getChild(instr_child_num), expected_return_type);
        s.popCodeBlock();
        mainScope = false;
    }
    
    /**Checks an ATN declaration
        @pre: This ATN is already defined by define_global_names
    */
    private void check_atn(ArlTree atn) {
      String id = atn.getChild(0).getText();
      if(!(s.getSymbol(id) instanceof ATNSymbol)) return;
      
      atnScope = true;
      s.pushCodeBlock();
      
      ArlTree atn_body = atn.getChild(1);
      // check ordered declaration of states, initial and accepting
      // (order defined and checked in syntax analysis)
      Set<String> setStates = new HashSet<String>();
      List<ArlTree> states = atn_body.getChild(0).getChildren();
      if (states == null) {
          new SemanticError("No states defined in ATN '"+id+"'.", atn_body.getChild(0), true);
          globError = true;
      } else {
        for (ArlTree t : states) {
          if (setStates.contains(t.getText())) {
            new SemanticError("State '"+t.getText()+"' duplicated in ATN states.", t, true);
            globError = true;
          } else if (stateIsDeclared(t)) { //check duplicated
            new SemanticError("Can't define state '"+t.getText()+"' because it's already defined.", t, true);
            globError = true; atnScope = false; return;
          } else {
            StateSymbol sym = new StateSymbol(t.getText());
            setStates.add(t.getText());
            s.defineSymbol(sym.id, sym);
          }
        }
      }
      
      List<ArlTree> initial = atn_body.getChild(1).getChildren();
      if (initial == null) {
          new SemanticError("No initial state defined in ATN '"+id+"'.", atn_body.getChild(1), true);
          globError = true;
      } else {
        Set<String> setInitial = new HashSet<String>();
        for (ArlTree t : initial)
          if (!stateIsDeclared(t)) {
            new SemanticError("State '"+t.getText()+"' not defined, can't be used as initial state.", t, true);
            globError = true;
          } else if (setInitial.contains(t.getText())) {
            new SemanticError("State '"+t.getText()+"' duplicated in initial states.", t, true);
            globError = true;
          } else
            setInitial.add(t.getText());
      }
      
      List<ArlTree> accept = atn_body.getChild(2).getChildren();
      if (accept == null) {
          new SemanticError("No accepting state defined in ATN '"+id+"'.", atn_body.getChild(2), true);
          globError = true;
      } else {
        Set<String> setAcc = new HashSet<String>();
        for (ArlTree t : accept)
          if (!stateIsDeclared(t)) {
            new SemanticError("State '"+t.getText()+"' not defined, can't be used as accepting state.", t, true);
            globError = true;
          } else if (setAcc.contains(t.getText())) {
            new SemanticError("State '"+t.getText()+"' duplicated in accepting states.", t, true);
            globError = true;
          } else
            setAcc.add(t.getText());
      }
      
      List<ArlTree> children = atn_body.getChildren();
      Set<String> setDefs = new HashSet<String>();
      for (ArlTree t: children) {
        if (t.getType() == ArlLexer.DECLARATION) {
          //atnScope is set to false in order to detect incorrect 
          //use of 'input' keyword during check_instruction
          atnScope = false;
          check_instruction(t);
          atnScope = true;
        }
        else if (t.getType() == ArlLexer.STATE) {
          String name = t.getChild(0).getText();
          if (t.getChild(0).getType() == ArlLexer.LOGIC) {
              name = t.getChild(1).getText();
          }
          if (setDefs.contains(name)) {
              new SemanticError("State '"+name+"' already defined", t, true);
              globError = true;
          } else {
              setDefs.add(name);
              check_state(t);
          }
          setStates.remove(name);
        }
      }
      s.popCodeBlock();
      
      for (String stt : setStates) {
        new SemanticWarning("State '"+stt+"' declared but not defined. Consider adding at "+
                            "least an empty definition: state "+stt+" {}", atn);
      }
      
      atnScope = false;
    }

    /** Performs type checking for a state of the ATN.
         @pre 'state' is a state node.
         @post throws semantic error messages if necessary
    */
    private void check_state(ArlTree state) {
        //handle logic states first and then leave the AST as if
        //they don't exist, removing the child.
        if (state.getChild(0).getType() == ArlLexer.LOGIC) {
            state.deleteChild(0); 
            state.setLogicState(); 
        }

      List<ArlTree> children = state.getChildren();
      if (children == null)
        new SemanticError("check_state error", state);
      
      if (!stateIsDeclared(children.get(0))) {
        new SemanticError("Can't define state '"+children.get(0).getText()+
                          "' because it's not declared in its ATN.", children.get(0), true);
        globError = true; return;
      }
        
      if (children.size() > 1) {
        List<ArlTree> transitions = children.get(1).getChildren();
        if (transitions != null) {
          for (ArlTree t: transitions) {
            Type ty = check_expression(t.getChild(0));
            if (ty.raw_type != RawType.BOOLEAN && ty.raw_type != RawType.REGEX) {
              new SemanticError("Transition condition must be boolean expression, ATN,"+
                                " regex or combination of those.", t.getChild(0), true);
              globError = true; 
            }
            if (!stateIsDeclared(t.getChild(1))) {
              new SemanticError("Transition to undeclared state '"+
                                t.getChild(1).getText()+"'.", t.getChild(1), true);
              globError = true;
            }
            if (t.getChildCount() > 2)
              check_instruction_block(t.getChild(2), null);
          }
        }
        
        if (children.size() > 2 && children.get(2).getChildCount() == 1)
          check_instruction_block(children.get(2).getChild(0), null);
      }
    }
    
    /**Returns true if the state name represented by t is already a defined symbol
        @pre: t points to a node that contains the name of a state
    */
    private boolean stateIsDeclared(ArlTree t) {
      return s.getSymbol(t.getText()) != null;
    }

    /** Performs type checking for the instruction represented by instr */
    private void check_instruction(ArlTree instr) {check_instruction(instr, null);}
    private void check_instruction(ArlTree instr, Type expected_return_type) {

        switch(instr.getType()) {
            case ArlLexer.WHILE:
            case ArlLexer.IF:
                String instr_name = (instr.getType() == ArlLexer.IF ? "if":"while");
                for(ArlTree child : instr.getChildren()) {
                    if(child.getType() == ArlLexer.INSTR_LIST) {
                        if (child.getChildCount() > 0) {
                            s.pushCodeBlock();
                            check_instruction_block(child, expected_return_type);
                            s.popCodeBlock();
                        }
                    }
                    else {
                        //This must be a condition expression
                        Type t = check_expression(child);
                        if(t.raw_type != RawType.BOOLEAN) {
                            new SemanticError(instr_name+"'s condition is "+t.toString()+
                                                    " but should be a boolean expression", child);
                        }
                    }
                }
                break;
            case ArlLexer.DECLARATION:
                VariableSymbol decl = new VariableSymbol();
                decl.type = instr.getChild(0).getTypeValue(); 
                decl.id = instr.getChild(1).getText();

                Symbol shadowed = s.getSymbol(decl.id);   

                if(shadowed != null && s.is_on_this_scope(decl.id)) 
                    new SemanticError("Redefinition of "+decl.id, instr.getChild(1));

                decl.is_global = false;
                s.defineSymbol(instr.getChild(1).getText(), decl);
                if(instr.getChildCount() == 3) { //We have an initial value
                    Type ini_type = check_expression(instr.getChild(2));
                    if(!decl.type.equals(ini_type)) new SemanticError("Trying to initialize a variable of type "
                            +decl.type.toString()+" with type "+ini_type.toString(), instr);
                }
                break;
            case ArlLexer.ASSIGN:
                if(instr.getChild(0).getType() != ArlLexer.ID) {
                    new SemanticError("Can't assign a value to a non-variable expression", instr);
                }
                Type lhs = check_expression(instr.getChild(0));
                Type rhs = check_expression(instr.getChild(1));
                if(!lhs.equals(rhs)) new SemanticError("Trying to assign "+rhs.toString()+
                              " to a variable of type "+lhs.toString(), instr.getChild(1));
                break;
            case ArlLexer.WRITE:
                Type write_t = check_expression(instr.getChild(0));
                if(write_t.raw_type != RawType.STRING) {
                    new SemanticError("write instruction expected a string, but got "+write_t.toString(), instr);
                }
                break;    
            case ArlLexer.CALL:
                call_check(instr);
                break;
            case ArlLexer.FUNCTION_CALL:
                check_expression(instr);
                break;
            default: 
                break;
        }
    }

    /**Checks the case for multi dimensional array and map access*/
    private void check_multi_dim_assign(ArlTree instr) { 
        Type assigned = check_expression(instr.getChild(1));
        String id = instr.getChild(0).getText();
        VariableSymbol vsym = (VariableSymbol) s.getSymbol(id);
        Type type = vsym.type;
        for(ArlTree dim : instr.getChild(0).getChildren()) {
            Type dim_t = check_expression(dim);
            
        }
    }
    
    /**Returns the type of the expression represented by expr*/
    private Type check_expression(ArlTree expr) { 
        Type t = new Type();
        switch(expr.getType()) {
            case ArlLexer.PLUS:
            case ArlLexer.NOT:
                if(expr.getChildCount() == 1) t = check_unary_op(expr);
                else  t = check_binary_op(expr);
                break;
            case ArlLexer.DOT:
            case ArlLexer.AND:
            case ArlLexer.OR:
            case ArlLexer.CMP:
                t = check_binary_op(expr);
                break;
            case ArlLexer.INT: 
                t.raw_type = RawType.INTEGER;
                break;
            case ArlLexer.BOOL: 
                t.raw_type = RawType.BOOLEAN;
                break;
            case ArlLexer.FLOAT: 
                t.raw_type = RawType.FLOAT;
                break;
            case ArlLexer.EXISTS:
                ArlTree object = expr.getChild(0);
                VariableSymbol sym2 = (VariableSymbol) s.getSymbol(object.getText());
                Type tp2 = check_expression(expr.getChild(1));
                if (object.getType() == ArlLexer.INPUT) {
                    if (!atnScope) {
                        new SemanticError("Keyword 'input' cannot appear outside ATN states", expr, true);
                        globError = true;
                    } 
                    if (tp2.raw_type != RawType.STRING && tp2.raw_type != RawType.REGEX) {
                        new SemanticError("Expected STRING or REGEX in 'exists' keyword second argument, " +
                                          "but found: " + tp2.toString(), expr, true);
                        globError = true;
                    }
                }
                else if (sym2 == null) {
                    new SemanticError("In keyword 'exists' first argument: '" + object.getText() + 
                                      "' has not been declared.", expr, true);
                    globError = true;
                } else if (sym2.type.raw_type != RawType.MAP) {
                    new SemanticError("In keyword 'exists', first argument must be input or map identifier.", expr, true);
                    globError = true;
                } else if (!sym2.type.contained_key_type.equals(tp2)) {
                    new SemanticError("In keyword 'exists', for first argument map, second argument must have"+
                                      " same type as map keys.", expr, true);
                    globError = true;
                }
                
                t.raw_type = RawType.BOOLEAN;
                break;
            case ArlLexer.INPUT:
                if (!atnScope) {
                  new SemanticError("Keyword 'input' cannot appear outside ATN states", expr, true);
                  globError = true;
                } 
                Type tp = check_expression(expr.getChild(0));
                if (tp.raw_type != RawType.STRING && tp.raw_type != RawType.REGEX) {
                  new SemanticError("Expected STRING or REGEX in 'input' keyword, but found "+tp.toString(), expr, true);
                  globError = true;
                }
                t.raw_type = RawType.STRING;
                break;
            case ArlLexer.CALL:
                call_check(expr);
                t.raw_type = RawType.BOOLEAN;
                break;
            case ArlLexer.SIZE:
                Type size_t = check_expression(expr.getChild(0));
                if(size_t.raw_type != RawType.MAP && size_t.raw_type != RawType.ARRAY) {
                    new SemanticError("size instruction expected a container", expr);
                }
                t.raw_type = RawType.INTEGER;
                break;
            case ArlLexer.STRING:
                t.raw_type = RawType.STRING;
                break;
            case ArlLexer.REGEX:
                // if regex parent is CMP or input or exists, regex is string, 
                // else regex is for implicit input
                ArlTree arl_par = (ArlTree) expr.getParent();
                if (arl_par.getType() == ArlLexer.INPUT || arl_par.getType() == ArlLexer.CMP ||
                    arl_par.getType() == ArlLexer.EXISTS) {}
                else {
                    expr.setImplicitInput();
                }
                t.raw_type = RawType.REGEX;
                break;
            case ArlLexer.CAPTURE:
                t.raw_type = RawType.STRING;
                break;
            case ArlLexer.FUNCTION_CALL:
                String func_id = expr.getChild(0).getText();
                Symbol func_sym = s.getSymbol(func_id);
                if(func_sym == null) {
                    new SemanticError("Undeclared identifier "+func_id+" used as a function name", expr);
                }
                if(!(func_sym instanceof FunctionSymbol)) {
                    new SemanticError("Identifier "+func_id+" used as a function name, but it's not a function", expr);
                }
                
                FunctionSymbol function_symbol = (FunctionSymbol) func_sym;
                List<ArlTree> params = expr.getChild(1).getChildren();

                if(params == null && function_symbol.param_types.size() != 0) {
                    new SemanticError("Function "+func_id+" should be called with arguments", expr);
                }
                else if(params != null && function_symbol.param_types.size() == 0) {
                    new SemanticError("Function "+func_id+" doesn't take any arguments but it's being called with arguments", expr);
                }
                else if(params != null && params.size() != function_symbol.param_types.size()) {
                    new SemanticError("Function "+func_id+" expected "+function_symbol.param_types.size()+
                                      " parameters, but got "+params.size()+" instead", expr);
                }
                else if(params != null) {
                    int i = 0;
                    for(ArlTree param_expr : expr.getChild(1).getChildren()) {
                        Type param_type = check_expression(param_expr);
                        if(!function_symbol.param_types.get(i).equals(param_type)) {
                            new SemanticError("Argument "+(i+1)+" for function "+func_id+" should be "+
                                              function_symbol.param_types.get(i).toString() + " but instead "+
                                              param_type.toString()+ " was given.", param_expr);
                        }
                        ++i; 
                    }
                }
                t = function_symbol.return_type; 
                break;
            case ArlLexer.ID:
                Symbol sym = s.getSymbol(expr.getText());   
                if(sym == null) new SemanticError("Undeclared identifier "+expr.getText(), expr);
                if (sym instanceof VariableSymbol) {
                    if(expr.getChildCount() > 0) {
                        VariableSymbol array_symbol = (VariableSymbol)sym;
                        if(array_symbol.type.raw_type != RawType.MAP &&
                           array_symbol.type.raw_type != RawType.ARRAY) {
                            new SemanticError("Can't index a "+array_symbol.type.toString(), expr);
                        }
                        else {
                            Type type = array_symbol.type;
                            for(ArlTree access : expr.getChildren()) {
                                if(type == null) {
                                    new SemanticError("Too many dimensions for "+array_symbol.id, expr);
                                }
                                Type access_type = check_expression(access);
                                if(type.raw_type == RawType.ARRAY) {
                                    if(access_type.raw_type != RawType.INTEGER) 
                                        new SemanticError("Arrays can only be indexed with integers", expr);
                                }
                                else if(type.raw_type == RawType.MAP) {
                                    if(!access_type.equals(type.contained_key_type)) {
                                        new SemanticError("Map key type is "+type.contained_key_type.toString()+
                                                          " But "+access_type.toString()+" was given instead", expr);
                                    }
                                }
                                type = type.contained_type;
                            }
                            t = type;
                        }
                    }
                    else {
                        t = ((VariableSymbol)sym).type;
                    }
                }
                else if (atnScope && sym instanceof ATNSymbol)
                    t.raw_type = RawType.BOOLEAN;
                else { // Error case
                    if (!atnScope)
                        new SemanticError(expr.getText()+" is not a variable.", expr);
                    else
                        new SemanticError("'"+expr.getText()+"' is not a ATN.", expr);
                }
                break;
            case ArlLexer.CAST:
                Type to_type = expr.getChild(0).getTypeValue();
                Type from_type = check_expression(expr.getChild(1));
                check_valid_cast(from_type, to_type, expr);
                t = to_type;
                break;
            case ArlLexer.ARRAY_V:
                t.raw_type = RawType.ARRAY;
                if(expr.getChildren() == null) new SemanticError("Array literals cannot be empty",expr); 
                Type chld_t0 = check_expression(expr.getChild(0));
                for(ArlTree chld : expr.getChildren()) {
                    Type chld_t = check_expression(chld); 
                    if(!chld_t.equals(chld_t0)) new SemanticError("Array types are not homogeneous, found "+
                            chld_t.toString()+" but first elements were "+chld_t0.toString(), chld);
                }
                t.contained_type = chld_t0;
                break;
            case ArlLexer.MAP_V:
                t.raw_type = RawType.MAP;
                Type cont0 = check_expression(expr.getChild(0).getChild(1));
                Type key0 = check_expression(expr.getChild(0).getChild(0));
                for(ArlTree pair : expr.getChildren()) {
                    Type cont = check_expression(pair.getChild(1));
                    if(!cont.equals(cont0)) new SemanticError("Value types are not homogeneous for map, found "+
                            cont.toString()+" but first elements were "+cont0.toString(), pair.getChild(1));
                    Type key = check_expression(pair.getChild(0));
                    if(!key.equals(key0)) new SemanticError("Key types are not homogeneous for map, found "+
                            key.toString()+" but first elements were "+key0.toString(), pair.getChild(0));
                }
                t.contained_type = cont0;
                t.contained_key_type = key0;
                break;
            case ArlLexer.EMPTY_CONTAINER:
                t.raw_type = RawType.EMPTY_CONTAINER;
                break;
        }
        return t;
    }
    
    private void call_check(ArlTree expr) {
        if (!mainScope) {
            new SemanticError("'call' keyword can only be used in main function.", expr, true);
            globError = true;
        }
        String id = expr.getChild(0).getText();
        Symbol symCall = s.getSymbol(id);
        if (symCall != null && symCall instanceof ATNSymbol) {}
        else {
            new SemanticError("Trying to call an ATN that doesn't exist: "+id, expr.getChild(0), true);
            globError = true;
        }
        if (expr.getChildCount() > 1) {
            Type exp = check_expression(expr.getChild(1));
            if (exp.raw_type == RawType.ARRAY && exp.contained_type.raw_type == RawType.MAP &&
                exp.contained_type.contained_type.raw_type == RawType.STRING &&
                exp.contained_type.contained_key_type.raw_type == RawType.STRING) {}
            else {
                new SemanticError("Call second argument when called with two arguments " +
                                  "must be map(string, string)[], but found "+ exp.toString(), expr.getChild(1), true);
                globError = true;
            }
        }
    }

    /**Checks if a cast is between valid types*/
    private void check_valid_cast(Type t1, Type t2, ArlTree cast) {
        if((!is_integral_type(t1) || !is_integral_type(t2)) && !((t1.raw_type == RawType.ARRAY || 
                        t1.raw_type == RawType.MAP) && t2.raw_type == RawType.STRING)) {
            new SemanticError("Invalid cast "+t1.toString()+" to "+t2.toString()+
                    ". Apart from container to string, only casting between integral, types is allowed", cast);
        }
    }

    /**Returns wether t is an integral type*/
    private boolean is_integral_type(Type t) {
        return !(t.raw_type == RawType.MAP || t.raw_type == RawType.ARRAY);
    }

    /**Returns wether t1 and t2 are compatible types*/
    private boolean is_compatible_type(Type t1, Type t2) {
        if(t1.raw_type == RawType.REGEX && t2.raw_type == RawType.REGEX)
            return false;
        if(t1.raw_type == RawType.STRING && t2.raw_type == RawType.REGEX ||
           t2.raw_type == RawType.STRING && t1.raw_type == RawType.REGEX) 
            return true;
        if(atnScope && (t1.raw_type == RawType.REGEX && t2.raw_type == RawType.BOOLEAN ||
           t2.raw_type == RawType.REGEX && t1.raw_type == RawType.BOOLEAN)) 
            return true;

        else return t1.equals(t2);
    }

    /**Returns wether op is a valid string binary operation*/
    private boolean valid_string_binary_op(String op) {
        return op.equals("+") || op.equals("==");
    }

    /**Returns the type of the binary operation, throws if incorrect.*/
    private Type check_binary_op(ArlTree expr) { 
        assert(expr.getChild(0) != null);
        assert(expr.getChild(1) != null);
        Type lhs = check_expression(expr.getChild(0));   
        Type rhs = check_expression(expr.getChild(1));   
        if(!is_integral_type(lhs) || !is_integral_type(rhs)) {
            new SemanticError("On operator "+expr.getText()+".\n    This operator can't be used on types "+
                    lhs.toString() + " and "+ rhs.toString() +" both types should be integral" , expr);
        }
        if(!is_compatible_type(lhs,rhs)) {
            new SemanticError("On operator "+expr.getText()+
                      ", incompatible types.\n    Left hand side is "+lhs.toString()+
                      " and right hand side is "+rhs.toString(), expr);
        }
        if((lhs.raw_type == RawType.STRING || rhs.raw_type == RawType.STRING) && 
            !valid_string_binary_op(expr.getText())) {
              new SemanticError("Operator "+expr.getText()+" not valid for strings.", expr, true);
              globError = true;
        }
        int et = expr.getType();
        if (et == ArlLexer.PLUS || et == ArlLexer.DOT) {
            return lhs;
        }
        else {
            Type ret = new Type();
            ret.raw_type = RawType.BOOLEAN;
            return ret;
        }
    }
    
    private Type check_unary_op(ArlTree expr) {
        assert(expr.getChild(0) != null);
        Type t = check_expression(expr.getChild(0));
        if(expr.getType() == ArlLexer.PLUS && t.raw_type != RawType.FLOAT &&
           t.raw_type != RawType.INTEGER) {
            new SemanticError("On operator "+expr.getText()+"(unary), intompatible type "+t.toString()+" expected int or float", expr);
        }
        if(expr.getType() == ArlLexer.NOT && t.raw_type != RawType.BOOLEAN) {
            new SemanticError("On operator "+expr.getText()+", intompatible type "+t.toString()+" expected bool", expr);
        }
        return t;
    }

    /**Builds a type structure from an ATN representing a type*/
    private Type build_type(ArlTree T) { 
        int n_chld = T.getChildCount();

        assert(T != null);
        assert(n_chld <= 2);

        Type type = new Type();

        if(T.getType() == ArlLexer.TYPE && n_chld == 0) { // Integral types
            String raw = T.getText();
            if(raw.equals("int")) type.raw_type = RawType.INTEGER;
            else if(raw.equals("float")) type.raw_type = RawType.FLOAT;
            else if(raw.equals("string")) type.raw_type = RawType.STRING;
            else if(raw.equals("bool")) type.raw_type = RawType.BOOLEAN;
            else type.raw_type = RawType.MAP;
        }
        else if (T.getType() == ArlLexer.MAP && n_chld == 2) { //Maps
            type.raw_type = RawType.MAP;
            type.contained_key_type = build_type(T.getChild(0));
            type.contained_type = build_type(T.getChild(1));
        }
        else if (T.getType() == ArlLexer.ARRAY_T && n_chld == 1) { //Arrays
            type.raw_type = RawType.ARRAY;
            type.contained_type = build_type(T.getChild(0));
        }
        else {
            new SemanticError("Unexpected error: Type definition is not valid", T);
        }
        return type;
    }
    
    private ArlTree getRoot(ArlTree t) {
        if (t != null) {
            if (t.getType() == ArlLexer.ROOT)
                return t;
            else if (t.getChildCount() == 1)
                return getRoot(t.getChild(0));
        }
        return null;
    }

    /**Prints information about the parsed program to see if everything works*/
    private void print_debug_info() {
        System.out.println("");
        System.out.println("===================");
        System.out.println("Program information");        
        System.out.println("===================");
        System.out.println("");


        List<FunctionSymbol> functions = s.getAllGlobalFunctions();
        System.out.println("Found "+functions.size()+" function declarations.");
        for(FunctionSymbol function : functions) {
            System.out.println(function.id+" :: "+ function.param_types.toString()+
                    (function.return_type == null ? "" : (" -> "+function.return_type.toString())));
        }

        List<ATNSymbol> atns = s.getAllGlobalATNs();
        System.out.println("");
        System.out.println("Found "+atns.size()+" ATN declarations.");
        for(ATNSymbol atn : atns) {
            System.out.println(atn.id);
        }
        
        if (!globError) {
          System.out.println("");
          System.out.println("===================");
          System.out.println("Program execution");        
          System.out.println("===================");
          System.out.println("");
        }
    }

}

