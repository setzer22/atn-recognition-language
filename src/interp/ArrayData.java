package interp;

import parser.*;
import java.util.*;

public class ArrayData extends Data {

    public ArrayData(List<Data> value) {this.value = value;}

    public List<Data> value;

    @Override
    public Data operation(String op, Data d) {
        if(op.equals("get"))
            return get(d.intv());
        else if(op.equals("size"))
            return new IntegerData(size());
        else 
            throw new RuntimeException("Unsupported operator "+op+" for int type");
    }

    @Override
    public Data ternary_operation(String op, Data o1, Object o2) {
        if(op.equals("set")) {
            assert(o2 instanceof Data);
            set(o1.intv(), (Data)o2);
            return null;
        }
        else if (op.equals("ensure")) {
            assert(o2 instanceof Type);
            ensure_capacity(o1.intv(), (Type)o2);
            return null;
        }
        else 
            throw new RuntimeException("Unsupported operator "+op+" for int type");
    }

    /**pre: index is a valid index in the array*/
    public void set (int index, Data data) {
        value.set(index, data);
    }

    /**Returns the index-th element of the array, if index is outside the array's 
     * bounds it results in an execution error.*/
    public Data get (int index) {
        if(index >= value.size()) throw new RuntimeException("Array index out of bounds: "+index);
        return value.get(index); 
    }

    public int size () {
        return value.size();
    }

    /**Ensures the array has enough capacity to hold elements up to index*/
    public void ensure_capacity(int index, Type data) {
        while(index >= value.size()) {
            value.add(Data.get_default_value(data));
        }
    }

    @Override 
    public Data cast(Type t) {
        switch(t.raw_type) {
            case STRING: return new StringData(value.toString());
            default: throw new RuntimeException("Invalid cast, int to "+t.toString());
        }
    }
    
    public Data clone() {
        List<Data> cloned_data = new ArrayList<Data>();
        for(Data d : value) {
            //Null is the emtpy container and must be treated separately
            if(d != null) 
                cloned_data.add(d.clone());
            else 
                cloned_data.add(null);
        }
        return new ArrayData(cloned_data);
    }
}
