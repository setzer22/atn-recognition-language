package interp;

import parser.*;
import java.util.*;

public class SymbolTable {
	private LinkedList<HashMap<String, Symbol>> symbol_record;
	private HashMap<String, Symbol> current_symbols;

	private HashMap<String, Symbol> global_symbols;

	public SymbolTable () {
		symbol_record = new LinkedList<HashMap<String, Symbol>>();
		current_symbols = new HashMap<String, Symbol>();
		symbol_record.push(current_symbols);
		global_symbols = new HashMap<String, Symbol>();
	}

	public void pushCodeBlock() {
		HashMap<String, Symbol> m = new HashMap<>();
		symbol_record.push(m);
		current_symbols = m;
	}

	/**Pops the current block, throws runtime exception if 
       no current block*/
	public void popCodeBlock() {
		try {
			symbol_record.pop();
			current_symbols = symbol_record.peekFirst();
		} catch (NoSuchElementException e) {
			throw new RuntimeException("Tried to pop an empty symbol table's stack");
		}
	}

    public List<FunctionSymbol> getAllGlobalFunctions() {
        List<FunctionSymbol> ret = new ArrayList<>();
        for(Symbol s : global_symbols.values()) {
            if(s instanceof FunctionSymbol) ret.add((FunctionSymbol)s);
        }
        return ret;
    }

    public List<ATNSymbol> getAllGlobalATNs() {
        List<ATNSymbol> ret = new ArrayList<>();
        for(Symbol s : global_symbols.values()) {
            if(s instanceof ATNSymbol) ret.add((ATNSymbol)s);
        }
        return ret;
    }

	public void defineSymbol(String id, Symbol symbol) {
		current_symbols.put(id, symbol);	
	}

	public void defineGlobalSymbol(String id, Symbol symbol) {
		global_symbols.put(id, symbol);
	}
	
	public Symbol getGlobalSymbol(String id) {
       return global_symbols.get(id);
    }

	/**Returns symbol if found anywhere, null if not found*/
	public Symbol getSymbol(String id) {
		for(HashMap<String, Symbol> ar : symbol_record) {
			Symbol s = ar.get(id);
			if(s != null) return s;			
		}	
		return getGlobalSymbol(id);
	}

	public boolean is_on_this_scope(String id) {
        Symbol s = current_symbols.get(id);
        if(s != null) return true;
        else return false;
	}
}

