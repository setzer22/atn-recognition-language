package interp;

import parser.*;

public class IntegerData extends Data {

    public IntegerData(int value) {this.value = value;}

    public int value;

    @Override
    public Data operation(String op, Data d) {
        assert(d instanceof IntegerData || d == null);

        //Int to int operations
        if(op.equals("+")) {
            if(d == null) 
                return new IntegerData(value);
            else 
                return new IntegerData(value + ((IntegerData)d).value);
        }
        else if(op.equals("-")) {
            if(d == null) 
                return new IntegerData(-value);
            else 
                return new IntegerData(value - ((IntegerData)d).value);
        }
        else if(op.equals("*"))
            return new IntegerData(value * ((IntegerData)d).value);
        else if(op.equals("/"))
            return new IntegerData(value / ((IntegerData)d).value);
        //Comparison operations
        else if(op.equals("=="))
            return new BooleanData(value == ((IntegerData)d).value);
        else if(op.equals("<"))
            return new BooleanData(value <  ((IntegerData)d).value);
        else if(op.equals(">"))
            return new BooleanData(value >  ((IntegerData)d).value);
        else if(op.equals(">="))
            return new BooleanData(value >= ((IntegerData)d).value);
        else if(op.equals("<="))
            return new BooleanData(value <= ((IntegerData)d).value);
        else 
            throw new RuntimeException("Unsupported operator "+op+" for int type");
    }

    @Override 
    public Data cast(Type t) {
        switch(t.raw_type) {
            case INTEGER: return new IntegerData(value);
            case FLOAT: return new FloatData((double)value);
            case BOOLEAN: return new BooleanData(value == 0 ? false : true);
            case STRING: return new StringData(Integer.toString(value));
            default: throw new RuntimeException("Invalid cast, int to "+t.toString());
        }
    }
    
    public Data clone() {
      return new IntegerData(value);
    }

    @Override
    public int hashCode() {
        return new Integer(value).hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof IntegerData)) return false;
        return (new Integer(value)).equals(((IntegerData)o).value);
    }
}
