package interp;

import org.antlr.runtime.tree.*;
import org.antlr.runtime.Token;

import java.util.List;

public class ArlTree extends CommonTree {

    private int intValue;
    private double floatValue;
    private String strValue;
    private Type typeValue;
    private boolean is_logic_state;
    private boolean is_implicit_input;
    
    public ArlTree(Token t) {
        super(t);
    }

    public ArlTree getChild(int i) {
        return (ArlTree) super.getChild(i);
    }

    @SuppressWarnings("unchecked")
    public List<ArlTree> getChildren() {
        return (List<ArlTree>) super.getChildren();
    }
    public int getIntValue() { return intValue;}

    public void setIntValue() { intValue = Integer.parseInt(getText()); }

    public boolean getBooleanValue() { return intValue != 0; }

    public double getFloatValue() {return floatValue;}

    public void setBooleanValue() {
        intValue = getText().equals("true") ? 1 : 0;
    }

    public void setFloatValue() {
        floatValue = Double.parseDouble(getText());
    }

    public String getStringValue() { return strValue; }
    public String getRegexValue() { return strValue; }

    public void setStringValue() {
        String s = getText();
        // Do not store the " at the extremes of the string
        strValue = s.substring(1,s.length()-1);
    }

    public void setRegexValue() {
        String s = getText();
        // Do not store the " at the extremes of the string
        strValue = s.substring(2,s.length()-1);
    }

    public void setLogicState() {
        is_logic_state = true;
    }
    public boolean getLogicState() {
        return is_logic_state;
    }
    
    public void setImplicitInput() {
        is_implicit_input = true;
    }
    public boolean getImplicitInput() {
        return is_implicit_input;
    }

    /**Once the type for the tree has been processed we set it and cut the 
     * remaining tree.*/
    public void setTypeValue(Type t) {
        typeValue = t; 
        for(int i = 0; i < getChildCount(); ++i) {
            deleteChild(i);
        }
    }

    public Type getTypeValue() { return typeValue; }
}
