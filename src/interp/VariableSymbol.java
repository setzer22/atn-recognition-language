package interp;

import parser.*;

public class VariableSymbol extends Symbol {
	public Type type;
    boolean is_parameter = false;
    boolean is_global = false;
    boolean is_atn_var = false;
    Data data;

    public String toString() {
        return id;
    }
}

