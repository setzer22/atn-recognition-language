package interp;

/** Class that holds transitions data. */
public class Transition {

    public String nextState;
    public ArlTree condition;
    public ArlTree actions;
}