package interp;

import java.util.HashMap;
import java.util.Stack;
import java.util.ListIterator;
import java.util.Set;

public class History {
    private DataInput input;

    private Stack<HashMap<VariableSymbol, Data>> varsStateStack;
    private Stack<Integer> backCountStack;

    private boolean merge_requested = false;
    
    public History (DataInput input) {
        varsStateStack = new Stack<>();
        backCountStack = new Stack<>();
        this.input = input;
    }
    
    public void reset(DataInput input) {
        varsStateStack = new Stack<>();
        backCountStack = new Stack<>();
        this.input = input;
    }

    public void request_merge() {
        assert(!merge_requested);
        merge_requested = true;
    }

    public void merge_if_requested() {
        if(merge_requested) {
            pop_merge();
            merge_requested = false;
        }
    }
    
    public void push(int input_steps) {
        varsStateStack.push(new HashMap<VariableSymbol, Data>());
        backCountStack.push(new Integer(input_steps));
    }
    
    /**Pops the history stack and undoes all changes made to the 
     * symbol table done before that push*/
    public void pop_undo() {
      if (!varsStateStack.empty()) {
        HashMap<VariableSymbol, Data> map = varsStateStack.pop();
        Set<VariableSymbol> set = map.keySet();
        for (VariableSymbol sym : set) {
            sym.data = map.get(sym);
        }

        Integer inputBack = backCountStack.pop();
        for(int i = 0; i < inputBack; ++i) input.back();
      }
    }

    /**Pops the history stack and all the variables that were present in 
       the popped state but not in the new are now present in the history's top */
    public void pop_merge() {
        assert(!varsStateStack.empty());
        HashMap<VariableSymbol, Data> weak   = varsStateStack.pop();
        assert(!varsStateStack.empty());
        HashMap<VariableSymbol, Data> strong = varsStateStack.peek();
        Set<VariableSymbol> weak_set   = weak.keySet();
        for (VariableSymbol sym : weak_set) {
            if(!strong.containsKey(sym)) 
                strong.put(sym, weak.get(sym));
        }

        Integer old = backCountStack.pop();
        Integer top = backCountStack.pop();
        backCountStack.push(top + old);
    }
    
    public void saveIfNeeded(VariableSymbol sym) {
      if (sym.is_global || sym.is_atn_var)
        if (!varsStateStack.empty() && !varsStateStack.peek().containsKey(sym)) {
            if(sym.data != null) 
                varsStateStack.peek().put(sym, sym.data.clone());
            else 
                varsStateStack.peek().put(sym, null);
                
        }
    }

    public void stackPrint() {
        ListIterator it = varsStateStack.listIterator(varsStateStack.size());
        while(it.hasPrevious()) {
            System.out.println(it.previous().toString());
        }
    }

}
