package interp;

public class SemanticError {
    public SemanticError(String m, ArlTree t) {
      System.out.println("(line "+t.getLine()+", col "+t.getCharPositionInLine()+") "+"ERROR: "+m);
      System.exit(0);
    }
    
    public SemanticError(String m, ArlTree t, boolean cont) {
      System.out.println("(line "+t.getLine()+", col "+t.getCharPositionInLine()+") "+"ERROR: "+m);
      if (!cont) System.exit(0);
    }
}
