package interp;

import parser.*;

import java.util.HashMap;
import java.util.ArrayList;

/** Class that holds ATN data. */
public class ATN {
    public ArrayList<String> states;
    public ArrayList<String> initStates;
    public ArrayList<String> acceptingStates;
    public ArrayList<String> logicStates;
    public ArrayList<ArlTree> declarations;
    public HashMap<String, ArrayList<Transition>> transitionsTable;
    public HashMap<String, ArlTree> statesActions;
    
    public String descr() {
      String s = "states:\n";
      for (String st : states)
        s += st+"\n";
      s += "initStates:\n";
      for (String st : initStates)
        s += st+"\n";
      s += "acceptingStates:\n";
      for (String st : acceptingStates)
        s += st+"\n"; 
      return s;
    }
}
