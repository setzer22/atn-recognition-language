
package interp;

import parser.*;

public class FloatData extends Data {

    public FloatData(double value) {this.value = value;}
    public double value;

    @Override
    public Data operation(String op, Data d) {
        assert(d == null || d instanceof FloatData);

        //Int to int operations
        if(op.equals("+")) {
            if(d == null) 
                return new FloatData(value);
            else 
                return new FloatData(value + ((FloatData)d).value);
        }
        else if(op.equals("-")) {
            if(d == null) 
                return new FloatData(-value);
            else 
                return new FloatData(value - ((FloatData)d).value);
        }
        else if(op.equals("*"))
            return new FloatData(value * ((FloatData)d).value);
        else if(op.equals("/"))
            return new FloatData(value / ((FloatData)d).value);
        //Comparison operations
        else if(op.equals("=="))
            return new BooleanData(value == ((FloatData)d).value);
        else if(op.equals("<"))
            return new BooleanData(value <  ((FloatData)d).value);
        else if(op.equals(">"))
            return new BooleanData(value >  ((FloatData)d).value);
        else if(op.equals(">="))
            return new BooleanData(value >= ((FloatData)d).value);
        else if(op.equals("<="))
            return new BooleanData(value <= ((FloatData)d).value);
        else 
            throw new RuntimeException("Unsupported operator "+op+" for float type");
    }

    @Override
    public Data cast(Type t) {
        switch(t.raw_type) {
            case INTEGER: return new IntegerData((int)value);
            case FLOAT: return new FloatData(value);
            case BOOLEAN: return new BooleanData(value == 0 ? false : true);
            case STRING: return new StringData(Double.toString(value));
            default: throw new RuntimeException("Invalid cast, float to "+t.toString());
        }
    }
    
    public Data clone() {
      return new FloatData(value);
    }

    @Override
    public int hashCode() {
        return new Float(value).hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof FloatData)) return false;
        return (new Float(value)).equals(((FloatData)o).value);
    }
}
