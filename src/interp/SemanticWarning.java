package interp;

public class SemanticWarning {
    public SemanticWarning(String m, ArlTree t) {
      System.out.println("(line "+t.getLine()+", col "+t.getCharPositionInLine()+") "+"WARNING: "+m);
    }
}
