package interp;

import parser.*;
import java.util.*;

public class MapData extends Data {

    public MapData(Map<Data, Data> value) {this.value = value;}

    public Map<Data, Data> value;

    @Override
    public Data operation(String op, Data d) {
        if(op.equals("get"))
            return get(d);
        if(op.equals("size"))
            return new IntegerData(size());
        else 
            throw new RuntimeException("Unsupported operator "+op+" for map type");
    }

    @Override
    public Data ternary_operation(String op, Data key, Object val) {
        if(op.equals("set")) {
            assert(val instanceof Data);
            set(key, (Data)val);
            return null;
        }
        else if(op.equals("ensure")) {
            assert(val instanceof Type);
            set(key, Data.get_default_value((Type)val));
            return null;
        }
        else 
            throw new RuntimeException("Unsupported operator "+op+" for map type");
    }

    public void set (Data key, Data val) {
        value.put(key, val);
    }

    public Data get (Data key) {
        Data val = value.get(key); 
        if(val == null) throw new RuntimeException("Key "+key.toString()+" is not present in the map");
        return val;
    }
    
    public boolean exists (Data key) {
        return value.containsKey(key);
    }

    public int size () {
        return value.keySet().size();
    }

    @Override 
    public Data cast(Type t) {
        switch(t.raw_type) {
            case STRING: return new StringData(value.toString());
            default: throw new RuntimeException("Invalid cast, map to "+t.toString());
        }
    }
    
    public Data clone() {
        Map<Data, Data> cloned_data = new HashMap<Data, Data>();
        for(Data d : value.keySet()) {
            //Null is the emtpy container and must be treated separately
            Data val = value.get(d); 
            if(val != null) 
                cloned_data.put(d.clone(), value.get(d).clone());
            else 
                cloned_data.put(d.clone(), null);
        }
        return new MapData(cloned_data);
    }
}
