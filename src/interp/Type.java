package interp;

import parser.*;
import java.util.List;

/**Class to represent a type, a type can contain an arbitrary
   number of nested types in arrays or maps; In that case 
   the contained_* variables are accordingly set*/
public class Type {
	public Type contained_type; //Null for integral types
	public Type contained_key_type; //Null for non-maps
	public RawType raw_type;	

	/**t1.equals(t2) means t1 and t2 are the same type*/
	public boolean equals(Type t) {
        if(t == null) return false;
		if (raw_type == RawType.ARRAY) {
			return (t.raw_type == RawType.ARRAY && contained_type.equals(t.contained_type)) || 
                   t.raw_type == RawType.EMPTY_CONTAINER;
		}
		else if (raw_type == RawType.MAP) {
			return (t.raw_type == RawType.MAP && contained_type.equals(t.contained_type) && contained_key_type.equals(t.contained_key_type)) ||
                   t.raw_type ==  RawType.EMPTY_CONTAINER;
		}
        else if (raw_type == RawType.EMPTY_CONTAINER) {
            return t.raw_type == RawType.MAP || t.raw_type == RawType.ARRAY || t.raw_type == RawType.EMPTY_CONTAINER;
        }
        else {
            return t.raw_type == raw_type;
        }
	}

    public String toString() {
        String s = "";
        if(raw_type == null) {
           return "uninitialized type";
        }
        else if(raw_type != RawType.MAP && raw_type != RawType.ARRAY) {
            s = rawToString(raw_type);    
        }
        else if (raw_type == RawType.ARRAY) {
            s = array_to_string(this);
        }
        else if (raw_type == RawType.MAP) {
            s += "map(";
            s += contained_key_type.toString();
            s += ", ";
            s += contained_type.toString();
            s += ")";
        }
        return s;
    }

    private static String array_to_string(Type t) {
        if(t.raw_type != RawType.ARRAY) return t.toString();
        else {
            String s = array_to_string(t.contained_type);
            s += "[]";
            return s;
        }
    }

    private static String rawToString(RawType r) {
        switch(r) {
            case INTEGER: return "int";
            case FLOAT: return "float";
            case BOOLEAN: return "bool";
            case STRING: return "string";
            case REGEX: return "regex";
            default: return r.toString();
        }
    }
}

