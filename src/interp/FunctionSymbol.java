package interp;

import parser.*;
import java.util.List;

public class FunctionSymbol extends Symbol {
	public Type return_type;
	public List<Type> param_types; 
	public List<String> param_names;
    public ArlTree code;
	
}
