./bin/Arl -noexec -dot -ast out.dot $1
dot -Tpdf out.dot > out.pdf
xdg-open out.pdf 2> /dev/null
rm out.dot out.pdf
