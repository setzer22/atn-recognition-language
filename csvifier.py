#!/bin/python

import sys
import re

def usage():
    print "\n    usage: csvifier fitxer_entrada\n"
    sys.exit()

if len(sys.argv) != 2:
    usage()
    
input_file = sys.argv[1]
try:
    with open (input_file, 'rb') as myfile:
        text = myfile.read()
        text = re.sub('[\n.,;!?]', '', text);
        text_list = re.split(' +', text);
        for t in text_list:
            print "text="+t
except:
  print "\n    Error: unable to open file", input_file, "\n"
  sys.exit()
 

