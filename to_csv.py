#!/bin/python
import sys
import re

def get_word_punctuation(text):
    word = ''
    punctuation = ''
    i = 0
    while i < len(text):
      if re.match('[.,;!?:=]', text[i]) == None: #(){}[] not included
        word += text[i]
        i = i + 1
      else: 
        break
    
    if i < len(text):
      punctuation = text[i:]
      punctuation = punctuation.replace('=', '\=')
      punctuation = punctuation.replace(',', '\,')
    return (word, punctuation)

def usage():
    print "\n    usage: to_csv fitxer_entrada\n"
    sys.exit()

if len(sys.argv) != 2:
    usage()
    
input_file = sys.argv[1]
try:
    with open (input_file, 'rb') as myfile:
        text = myfile.read()
        text = re.sub('[\n\t]', '', text)
        text_list = re.split(' +', text)
        
        for i in range(len(text_list)):
            pair = get_word_punctuation(text_list[i])
            res = 'text=' + pair[0]
            if pair[1] != '':
              res += ',punctuation=' + pair[1]
            if i == len(text_list) - 1:
              res += ',EOF=true'
            print res
            
except:
  print "\n    Error: unable to open file", input_file, "\n"
  sys.exit()
 

